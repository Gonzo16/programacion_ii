"""
Entrega Practica 1 - Lucas Pedroso Heras - 3ºA BUSINESS ANALYTICS E INGENIERÍA INFORMÁTICA - Programación II
"""


class Order:

    def __init__(self, id_order: int, customer: str, raw_material: str, min_quality_level: int, min_height: float, max_height: float, min_quantity: int, max_quantity: int,min_cut_length: float,max_cut_length: float):
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length

        self.assigned_stock_pieces = set([])
    def get_assigned_quantity(self) -> float:
        return NotImplementedError

    def calc_delay(self) -> float:
        raise NotImplementedError


class StockPiece:

    def __init__(self, id_stock_piece: int,height: float,length: float,quality_level:int,position_x: float,position_y: float,id_father_stock_piece:int):

        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y
        self.id_father_stock_piece = id_father_stock_piece

        self.orders = {}

    def get_area(self) -> float:
        """Returns the area of the StockPiece calculated by multiplying the height by the length"""
        return self.height * self.length

    def checkresidual(self) -> float:
        raise NotImplementedError

    def assign_order(self, order):
        for orders in order:
            if order.min_height < self.height < order.max_height  and self.quality_level > order.min_quality_level:
                assigned_order= orders.id_order
                self.orders[orders.id_order] = assigned_order
                idstock_piece = self.id_stock_piece
                orders.assigned_stockpiece[self.id_stock_piece] = idstock_piece
                return self.orders[orders.id_order], orders.assigned_stockpiece[self.id_stock_piece]



class Map:
    def __init__(self,maptype: str,real: int):
        self.maptype = maptype
        self.real = real

        self.assigned_stockpieces = set([])
    def get_assigned_quantity(self)->float:
        raise NotImplementedError

    def calc_residual_area(self):
        raise NotImplementedError

    def add_stock_piece(self,StockPiece):
        stockpiece_assigned = StockPiece.id_stock_piece
        self.assigned_stockpieces.add(stockpiece_assigned)
        return stockpiece_assigned

    def delete_stock_piece(idStockPiece : int):
        raise NotImplementedError


class FabricSheet:

    def __init__(self, id_fabric_sheet: int,raw_material: str,height: float,length: float,department: str,weaving_forecast: str):

        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast

    def select_map(Map):
        raise NotImplementedError

    def add_map(Map):
        inicial = Map('Initial', 1)
        intermedio = Map('Intermediate', 1)
        final = Map('Final', 1)
        
        return inicial, intermedio, final